package com.example.hearttree;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HeartTreeApplication {

    public static void main(String[] args) {
        SpringApplication.run(HeartTreeApplication.class, args);
    }

}
