package com.example.hearttree;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @Classname IndexController
 * @Description TODO
 * @Date 2021/4/20 0020 9:49
 * @Created by ldy
 */
@Controller
public class IndexController {

    @GetMapping("/surprise")
    public String index(){
        return "index.html";
    }

}
